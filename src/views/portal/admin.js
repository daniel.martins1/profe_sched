
import React from 'react'
import { Nav, Navbar} from 'react-bootstrap'
import styled from 'styled-components'

const Admin = () => {


    return (
        <>
        <h1>This user have admin rights: "With great powers come great responsability." <br/>
        <NavStyled>
        <Navbar bg="primary" variant="dark">
    <Nav>
      <Nav.Link href="/admin/turma">Turmas</Nav.Link>
      <Nav.Link href="/admin/disciplina">  / Disciplinas</Nav.Link>
      <Nav.Link href="/admin/profe">  / Professores</Nav.Link>
    </Nav>
      </Navbar>
      </NavStyled>
      </h1><br/>

        </>
    )
}

export default Admin

const NavStyled = styled.div`
 
`

