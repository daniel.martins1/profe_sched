import React, { useEffect, useState } from 'react'
import { getTurma, deleteTurma  } from '../../../services/admin'
import { Button, Table } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2'
import moment from'moment'

const List = (props) => {
    const [products, setProducts] = useState([])
    const [isUpdate, setUpdate] = useState(false)
    // const [count, setCount] = useState(0);
    // useEffect(() => {

    //     let get = async () => {
    //          const obj = await getTurma(); 
    //          setProducts(obj.data); }
    //     if(count < 1) {
    //         setTimeout(() => {
    //           setCount(count + 1);
    //         }, 1000);
    //         get();

    //     }
          
    // })
    useEffect(() => {
        setUpdate(false)
        let get = async () => { const obj = await getTurma();
            setProducts(obj.data); }
        if (!isUpdate) {
            get();
        }
        return () => get = () => { };
    }, [isUpdate])

    const _deleteProduct = async (obj) => {
        const message = (type, message) => Swal.fire({
            position: 'top-end',
            icon: type || 'success',
            title: message || `Turma excluída.`,
            showConfirmButton: false,
            timer: 2500
        })

        Swal.fire({
            title: `Deseja excluir a turma de ${obj.disciplina.name} ?`,
            showCancelButton: true,
            confirmButtonText: `Sim`,
            cancelButtonText: `Não`,
        }).then((result) => {
            if (result.isConfirmed) {
                deleteTurma(obj._id)
                    .then(() => {
                        setUpdate(true)
                        message('success', `Turma de ${obj.title} excluída.`)
                    })
                    .catch(() => message('danger', `Erro ao excluir`))
            }
        })
    }

    return (
        <>

            <Table className="table">

                <thead>
                    <tr>
                    <THeadItem>Disciplina</THeadItem>
                     <THeadItem>Curso</THeadItem>
                    <THeadItem>Profe</THeadItem>
                    <THeadItem>Começa</THeadItem>
                    <THeadItem>Termina</THeadItem>
                    <THeadItem>Turno</THeadItem>
                    <THeadItem>Dias</THeadItem>
                    <THeadItem>Ação</THeadItem>
                    </tr>
                </thead>

                <tbody>
                    {products.map((obj, i) => (
                        <tr key={i}>
                        <td>{obj.disciplina.name}</td>
                        <td>{obj.disciplina.curso}</td>
                        <td>{obj.profe.name}</td>
                        <td>{moment(obj.comeca_date).format("YYYY MMM DD")}</td>
                        <td>{moment(obj.termina_date).format("YYYY MMM DD")}</td>
                        <td>{obj.turno}</td>
                        <td>{obj.dias}</td>
                        <td> <ActionButton onClick={() => props.updateCategory(obj)} variant="warning" size="sm">Editar</ActionButton>
                            | 
                            <ActionButton onClick={() => _deleteProduct(obj)} variant="danger" size="sm">Excluir</ActionButton></td>

                        </tr>
                    ))}

                </tbody>
            </Table>
        </>
    )

}

   
export default List

const THeadItem = styled.th`
    background: #666;
    color:#eee;
`
const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.4
    }
`       
