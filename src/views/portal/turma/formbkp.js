import React, { useState, useEffect } from 'react'
import {createTurma, updateTurma, getProfe, getDisciplina } from '../../../services/admin'
import { Button, Form } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2'


const CategForm = (props) => {
    
    const [disciplina, setDisciplina] = useState([])
    const [profe, setProfe] = useState([])
    const [formCategory, setFormCategory] = useState({...props.treconovo})

    const check = (data) => Object.keys(props.treconovo).length >0 
    ? updateTurma (props.treconovo._id, data)
    : createTurma (data)
    
    const check2 = Object.keys(props.treconovo).length >0 
    

    const handleChange = (attr) => {
        setFormCategory({
            ...formCategory,
            [attr.target.name]: attr.target.value
        })
    }


    const submete = async () => {
        const message = (type, message) => Swal.fire({
            position: 'top-end',
            icon: type || 'success',
            title: message || `Turma eliminada.`,
            showConfirmButton: false,
            timer: 2500
        })

        try {
            await check(formCategory)
            message('success', `Turma Cadastrada.`)
            if (!check2) {
                setFormCategory({})
            }
        } catch (error) {
            message('error', `Deu Merda.`)
        }

    }

    useEffect(() => {
        let get = async () => { 
            const d = await getDisciplina(); setDisciplina(d.data);
            const p = await getProfe(); setProfe(p.data)}
        get();
        //clear
        return () => get = () => { };
    }, [])

return (
        <>
            
             <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="comeca_date" value={formCategory.comeca_date || ""} placeholder="Início" />
            </Form.Group>
            <Form.Group >
                <Form.Control Type="text" onChange={handleChange} name="termina_date" value={formCategory.termina_date || ""} placeholder="Término" />
            </Form.Group>
            <Form>
  {['checkbox'].map((type) => (
    <div key={`inline-${type}`}>
      <Form.Check inline label="Seg" type={type} id={`inline-${type}-Seg`} />
      <Form.Check inline label="Ter" type={type} id={`inline-${type}-Ter`} />
      <Form.Check inline label="Qua" type={type} id={`inline-${type}-Qua`} />
      <Form.Check inline label="Qui" type={type} id={`inline-${type}-Qui`} />
      <Form.Check inline label="Sex" type={type} id={`inline-${type}-Sex`} />
      <Form.Check inline label="Sab" type={type} id={`inline-${type}-Sab`} />
      
    </div>
  ))}
</Form>
            <Form.Group >
                <Form.Control Type="text" onChange={handleChange} name="dias" value={formCategory.dias || ""} placeholder="Dias da semana" />
            </Form.Group>
                   <Form.Group controlId="exampleForm.ControlSelect1">
                <Form.Label>Curso</Form.Label>
                <Form.Control as="select" name="curso" onChange={handleChange} value={formCategory.curso || <option>Tem que escolher</option>}>

                    {disciplina.map((it, i) => (
                        <option key={i} value={it._id}>{it.curso}</option>
                    ))}
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlSelect1">
                <Form.Label>Disciplina</Form.Label>
                <Form.Control as="select" name="disciplina" onChange={handleChange} value={formCategory.disciplina || <option>Tem que escolher</option>}>

                    {disciplina.map((it, i) => (
                        <option key={i} value={it._id}>{it.name}</option>
                    ))}
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlSelect1">
                <Form.Label>Professor</Form.Label>
                <Form.Control as="select" name="profe" onChange={handleChange} value={formCategory.profe || <option>Tem que escolher</option>}>

                    {profe.map((it, i) => (
                        <option key={i} value={it._id}>{it.name}</option>
                    ))}
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlSelect1">
                <Form.Label>Turno</Form.Label>
                <Form.Control as="select" name="turno" onChange={handleChange} value={formCategory.turno || <option>Tem que escolher</option>}>
                <option>manha</option>
                <option>tarde</option>
                <option>noite</option>
                </Form.Control>
            </Form.Group>
            
            
 
            <Button variant="primary" onClick={submete} >
                Enviar
                </Button>
         
        </>
    )

    }
    export default  CategForm