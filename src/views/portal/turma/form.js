import React, { useState, useEffect } from 'react'
import { createTurma, updateTurma, getProfe, getDisciplina } from '../../../services/admin'
import { Button, Form } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//import moment from 'moment'

const CategForm = (props) => {

    const [disciplina, setDisciplina] = useState([])
    const [profe, setProfe] = useState([])
    const [formCategory, setFormCategory] = useState({
        ...props.treconovo,
        disciplina: props.treconovo?.disciplina?._id || undefined,
        profe: props.treconovo?.profe?._id || undefined,

    })
   const [startDate, setStartDate] = useState(new Date(props.treconovo.comeca_date || new Date() ));
   const [endDate, setEndDate] = useState(new Date(props.treconovo.termina_date  || new Date()  ));
//    const [startDate, setStartDate] = useState(new Date());
//    const [endDate, setEndDate] = useState(new Date());

    const decidemetodo = (data) => isUpdate ? updateTurma(props.treconovo._id, data) : createTurma(data)
    const isUpdate = Object.keys(props.treconovo).length > 0


    const handleChange = (attr) => {

        setFormCategory({
            ...formCategory,
            [attr.target.name]: attr.target.value
        })
        console.log(formCategory)
    }

    const setainicio = (date) => {
        setStartDate(date)
        setFormCategory({
            ...formCategory,
            comeca_date: date
        })
        console.log(formCategory)
    }

    const setatermino = (date) => {
        setEndDate(date)
        setFormCategory({
            ...formCategory,
            termina_date: date
        })
        console.log(formCategory)
    }


    const submete = async () => {
        const message = (type, message) => Swal.fire({
            position: 'top-end',
            icon: type || 'success',
            title: message || `Turma eliminada.`,
            showConfirmButton: false,
            timer: 2500
        })

        // decidemetodo(formCategory)
        try {
            await decidemetodo(formCategory)
            message('success', `Turma Cadastrada.`)
            // if (!check2) {
            //     setFormCategory({})
            // }
        } catch (error) {
            message('error', `Deu Merda.`)
        }
            // .then((res) => {

            //     message('success', `Turma Cadastrada`)
            // })
            // .catch((err) => message('error', `Deu Merda.`))

        setFormCategory({})

    }

    useEffect(() => {
        let get = async () => {
            const d = await getDisciplina(); setDisciplina(d.data);
            const p = await getProfe(); setProfe(p.data)
        }
        get();
        //clear
        return () => get = () => { };
    }, [])

    return (
        <div>

            <Formstyled>
                {/* Data de Início <DatePicker selected={startDate} name="comeca_date"onChange={date => setStartDate(date)} value={moment(formCategory.comeca_date).format("YYYY MMM DD")}/> */}
                Data de Início  <DatePicker selected={startDate} name="comeca_date" onChange={date => setainicio(date)} ></DatePicker>
                <br />
                <br />
            Data de Término  <DatePicker selected={endDate} name="termina_date" onChange={date => setatermino(date)}></DatePicker>
                <br />
                <br />

  <Form.Group size="lg" controlId="ControlSelect0">
                    <Form.Label>Dias da Semana  </Form.Label>
                    <Form.Control size="lg" as="select" name="dias" onChange={handleChange} value={formCategory.dias || ""}>
                    <option></option>    
                        <option>Seg, Qua</option>
                        <option>Ter, Qui</option>
                        <option>Seg, Ter, Qua, Qui, Sex</option>                  
                        <option>Sex</option>
                        <option>Sab</option>
                    </Form.Control>
                </Form.Group>
                <br />
                <Form.Group controlId="ControlSelect1">
                    <Form.Label>Disciplina  </Form.Label>
                    <Form.Control size="lg" as="select" name="disciplina" onClick={handleChange} value={formCategory.disciplina || ""}>
                    <option></option>    
                        {disciplina.map((it, i) => (
                            <option key={i} value={it._id}>{it.name}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
                <br />
                <Form.Group controlId="ControlSelect2">
                    <Form.Label>Curso  </Form.Label>
                    <Form.Control size="lg" as="select" name="curso"  value={formCategory.disciplina || ""}>
                    <option></option>    
                        {disciplina.map((it, i) => (
                            <option key={i} value={it._id}>{it.curso}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
                <br />
                <Form.Group controlId="ControlSelect3">
                    <Form.Label>Professor  </Form.Label>
                    <Form.Control size="lg" as="select" name="profe" onClick={handleChange} value={formCategory.profe || ""}>
                    <option></option>    
                        {profe.map((it, i) => (
                            <option key={i} value={it._id}>{it.name}</option>
                        ))}
                    </Form.Control>
                </Form.Group>
                <br />
                <Form.Group size="lg" controlId="ControlSelect4">
                    <Form.Label>Turno  </Form.Label>
                    <Form.Control size="lg" as="select" name="turno" onClick={handleChange} value={formCategory.turno || ""} placeholder={"turno"}>
                    <option></option>    
                        <option>manha</option>
                        <option>tarde</option>
                        <option>noite</option>
                    </Form.Control>
                </Form.Group>
            </Formstyled>
            <ButtonStyled>
                <Button variant="primary" onClick={submete} >
                    Enviar
                </Button>
            </ButtonStyled>

        </div>
    )

}
export default CategForm

const ButtonStyled = styled(Button)`
    padding:2px 4px;
    font-weight:500;
margin: 20px
`
const Formstyled = styled.div`

  font-size: 15px;
    padding:20px;
  background:beige
    `