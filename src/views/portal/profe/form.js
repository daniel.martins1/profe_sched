import React, { useState } from 'react'
import {createProfe, updateProfe } from '../../../services/admin'
import { Button, Form } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2'

const CategForm = (props) => {

    
    const [formCategory, setFormCategory] = useState({...props.treconovo})

    const check = (data) => Object.keys(props.treconovo).length >0 
    ? updateProfe (props.treconovo._id, data)
    : createProfe (data)
    
    const check2 = Object.keys(props.treconovo).length >0 
    

    const handleChange = (attr) => {
        setFormCategory({
            ...formCategory,
            [attr.target.name]: attr.target.value
        })
    }


    const submete = async () => {
        const message = (type, message) => Swal.fire({
            position: 'top-end',
            icon: type || 'success',
            title: message || `Professor excluído.`,
            showConfirmButton: false,
            timer: 2500
        })

        try {
            await check(formCategory)
            message('success', `Professor Cadastrado.`)
            if (!check2) {
                setFormCategory({})
            }
        } catch (error) {
            message('error', `Deu Ruim.`)
        }

    }



return (
        <>
            <Formstyled>
             <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="name" value={formCategory.name || ""} placeholder="Nome do Professor" />
            </Form.Group>
            <Form.Group >
                <Form.Control Type="text" onChange={handleChange} name="email" value={formCategory.email || ""} placeholder="e-mail" />
            </Form.Group>
            <Form.Group >
                <Form.Control Type="text" onChange={handleChange} name="password" value={formCategory.password || ""} placeholder="password" />
            </Form.Group>
            <Form.Group >
                <Form.Control Type="text" onChange={handleChange} name="isAdmin" value={formCategory.isadmin || "false"} placeholder="é admin?" />
            </Form.Group>
            </Formstyled>
 <ButtonStyled>
            <Button variant="primary" onClick={submete} >
                Enviar
                </Button>
                </ButtonStyled>
         
        </>
    )

    }
    export default  CategForm

    const ButtonStyled = styled(Button)`
    padding:2px 4px;
    font-weight:500;
margin: 20px
` 
const Formstyled = styled.div`

  font-size: 40px;
    padding:20px;
  background:beige
  `