import React from 'react'
import styled from 'styled-components'
import { useState } from "react"
import { Button, Form, Spinner, Figure } from 'react-bootstrap';
import { authentication } from '../../services/auth'
import http from '../../config/http';
import { saveToken } from '../../config/auth';
import history from '../../config/history'
import gato from '../imgs/capachogato.jpg'

const Login = () => {

  const [form, setForm] = useState("")
  const [loading, setLoading] = useState(false)

  const handleChange = (attr) => {
    setForm({
      ...form,
      [attr.target.name]: attr.target.value
    })
  }

  const isSubmitValid = () => form.email && form.password


  const submitLogin = async (e) => {
    e.preventDefault()
    if (isSubmitValid()) {
      setLoading(true)
      try {
        console.log(form)
        const { data } = await authentication(form)
        const { token } = data
        http.defaults.headers["x-auth-token"] = token;
        saveToken(token)
        history.push('/logado')

      } catch (error) {
        setLoading(false)
        console.log('deu ruim', error)
      }
    }
  }


  return (

    <>

      <FRUFRU>
        <Form>
          <div className="text-center">
            <h3>Login Docente</h3>
          </div>
          <Form.Group controlId="formBasicEmail">
            <Form.Control size="lg" onChange={handleChange} type="email" name="email" placeholder="e-mail" value={form.email || ""} />

          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Control size="lg" onChange={handleChange} type="password" name="password" placeholder="Password" value={form.password || ""} />
          </Form.Group>

          <Button onClick={submitLogin} disabled={!isSubmitValid()} className={`btn ${isSubmitValid() ? 'btn-primary' : "btn-secondary"} btn-user btn-block`} variant="primary" type="submit">

            {loading ? (
              <>
                <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                />
                                                        Carregando...
                                                    </>
            ) : "Login"}

          </Button>
        </Form>
      </FRUFRU>
      <IMGCosmetics>
        <Figure>
          <Figure.Image
            width={300}
            height={300}
            src={gato}
          />
          <Figure.Caption>
            "Vacilatum sum, enrabatum est."
  </Figure.Caption>
        </Figure>
      </IMGCosmetics>

    </>

  )
}
export default Login
const FRUFRU = styled.div`
background: lightgrey;
align-items: center;
text-align:center;
margin:30px;
padding: 50px;
justify-content: center;
`
const IMGCosmetics = styled.div`
display: flex;
background: lightgrey;
align-items: center;
text-align:center;
margin:30px;
padding: 50px;
justify-content: center;
`
