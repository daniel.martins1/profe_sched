import React, { useState } from 'react'
import { Button, Nav, Navbar} from 'react-bootstrap'
import styled from 'styled-components'
import List from './profe/list'
import CategForm from './profe/form'



const Profe = () => {


    const [isCreate, setCreate] = useState(false)
    const [treconovo, setTreconovo] = useState({})
    
    const updateCategory = (catg) => {
    setCreate(true)
    setTreconovo(catg)
    console.log(catg)
    
    }



    return (
        <>
        <h1>This user have admin rights: "With great powers come great responsability." <br/>
        <Navbar bg="primary" variant="dark">
    <Nav>
      <Nav.Link href="/admin/turma">Turmas</Nav.Link>
      <Nav.Link href="/admin/disciplina">  / Disciplinas</Nav.Link>
      <Nav.Link href="/admin/profe">  / Professores</Nav.Link>
    </Nav>
      </Navbar>
      </h1><br/>
      <ButtonStyled>
        <Button size="sm" onClick={() => setCreate(!isCreate)}>
                {isCreate ? "Volta pra Lista" : "Novo Prof."}
                </Button>
                </ButtonStyled>
 
        <hr />
        <Categories>
            {isCreate ? <CategForm treconovo={treconovo}/> : <List updateCategory={updateCategory}/>}
        </Categories>
        </>
    )
}

export default Profe

const Categories = styled.div`
 
`
const ButtonStyled = styled.button`
    padding:2px 4px;
    font-weight:500;
margin: 20px
`       

// const ActionButton = styled(Button)`
//     padding:2px 4px;
//     font-weight:500;

//     hover {
//         opacity:0.4
//     }
// `       