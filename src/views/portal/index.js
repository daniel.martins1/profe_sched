import React from 'react'
import Header from './header'
// import Login from './login'
import styled from 'styled-components';


const Layout = ({ children }) => {
    return (
        <>
            <HeaderStyle>
                <Header />
            </HeaderStyle>
            <ContentStyle>
                {children}
            </ContentStyle>

        </>
    )
}



const HeaderStyle = styled.div`
`
const ContentStyle = styled.div``

export default Layout
