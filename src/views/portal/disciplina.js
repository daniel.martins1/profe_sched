import React, { useState } from 'react'
//import { getCategories, deleteCategory } from '../../services/admin'
import { Button, Nav, Navbar } from 'react-bootstrap'
import styled from 'styled-components'
import List from './disciplina/list'
import CategForm from './disciplina/form'



const Disciplina = () => {


    const [isCreate, setCreate] = useState(false)
    const [treconovo, setTreconovo] = useState({})
    
    const updateCategory = (catg) => {
    setCreate(true)
    setTreconovo(catg)
    console.log(catg)
    
    }



    return (
        <>
                <h1>This user have admin rights: "With great powers come great responsability." <br/>
        <Navbar bg="primary" variant="dark">
    <Nav>
      <Nav.Link href="/admin/turma">Turmas</Nav.Link>
      <Nav.Link href="/admin/disciplina">  / Disciplinas</Nav.Link>
      <Nav.Link href="/admin/profe">  / Professores</Nav.Link>
    </Nav>
      </Navbar>
      </h1><br/>
      <ButtonStyled>
        <Button size="sm" onClick={() => setCreate(!isCreate)}>
                {isCreate ? "Volta pra Lista" : "Nova Disciplina"}
                </Button>
                </ButtonStyled>
        <hr />
        <Categories>
            {isCreate ? <CategForm treconovo={treconovo}/> : <List updateCategory={updateCategory}/>}
        </Categories>
        </>
    )
}

export default Disciplina

const Categories = styled.div`
 
`
const ButtonStyled = styled.button`
    padding:2px 4px;
    font-weight:500;
margin: 20px
`       
