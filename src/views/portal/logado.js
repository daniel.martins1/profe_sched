import React, { useEffect, useState } from 'react'
import { getTurma } from '../../services/admin'
import Table from 'react-bootstrap/Table'
import styled from 'styled-components'
import moment from 'moment'

const List = () => {
    const [products, setProducts] = useState([])
    const [count, setCount] = useState(0);

    useEffect(() => {

        let get = async () => {
             const obj = await getTurma(); 
             setProducts(obj.data); 
            }
        if(count < 1) {
            setTimeout(() => {
              setCount(count + 1);
            }, 1000);
            get();

        }
          
    })


    return (
        <>
            <Table className="table">

                <thead>
                    <tr>
                    <THeadItem>Disciplina</THeadItem>
                     <THeadItem>Curso</THeadItem>
                    <THeadItem>Profe</THeadItem>
                    <THeadItem>Começa</THeadItem>
                    <THeadItem>Termina</THeadItem>
                    <THeadItem>Turno</THeadItem>
                    <THeadItem>Dias</THeadItem>
                    </tr>
                </thead>

                <tbody>
                    {products.map((obj, i) => (
                        <tr key={i}>
                        <td>{obj.disciplina.name}</td>
                        <td>{obj.disciplina.curso}</td>
                        <td>{obj.profe.name}</td>
                        <td>{moment(obj.comeca_date).format("YYYY MMM DD")}</td>
                        <td>{moment(obj.termina_date).format("YYYY MMM DD")}</td>
                        <td>{obj.turno}</td>
                        <td>{obj.dias}</td>
                        </tr>
                    ))}

                </tbody>
            </Table>
        </>
    )

}

   
export default List

const THeadItem = styled.th`
    background: #666;
    color:#eee;
`
