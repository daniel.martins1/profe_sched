import React, { useState } from 'react'
import { createDisciplina, updateDisciplina } from '../../../services/admin'
import { Button, Form } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2'

const CategForm = (props) => {

    
    const [formCategory, setFormCategory] = useState({...props.treconovo})

    const check = (data) => Object.keys(props.treconovo).length >0 
    ? updateDisciplina (props.treconovo._id, data)
    : createDisciplina (data)
    
    const check2 = Object.keys(props.treconovo).length >0 
    

    const handleChange = (attr) => {
        setFormCategory({
            ...formCategory,
            [attr.target.name]: attr.target.value
        })
    }


    const submete = async () => {
        const message = (type, message) => Swal.fire({
            position: 'top-end',
            icon: type || 'success',
            title: message || `Disciplina excluída.`,
            showConfirmButton: false,
            timer: 2500
        })

        try {
            await check(formCategory)
            message('success', `Disciplina Cadastrada.`)
            if (!check2) {
                setFormCategory({})
            }
        } catch (error) {
            message('error', `Deu Ruim.`)
        }

    }



return (
        <>
            <Formstyled>
             <Form.Group className="Form">
                <Form.Control type="text" onChange={handleChange} name="name" value={formCategory.name || ""} placeholder="Disciplina" />
            </Form.Group>
            <Form.Group >
                <Form.Control Type="text" onChange={handleChange} name="curso" value={formCategory.curso || ""} placeholder="Curso" />
            </Form.Group>
            </Formstyled>
 <ButtonStyled>
            <Button variant="primary" onClick={submete} >
                Enviar
                </Button></ButtonStyled>

        </>

    )

    }
    export default  CategForm



const Formstyled = styled.div`

  font-size: 40px;
    padding:20px;
  background:beige
  `
  const ButtonStyled = styled(Button)`
    padding:2px 4px;
    font-weight:500;
margin: 20px
`       