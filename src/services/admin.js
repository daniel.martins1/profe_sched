import http from '../config/http'

//GET
const getProfe = () => http.get('/profe')
const getDisciplina = () => http.get('/disciplina')
const getTurma = () => http.get(`/turma`)
//const getTurma = (id) => http.get(`/turma/${id}`)


// DElELE
const deleteProfe = (id) => http.delete(`/profe/${id}`)
const deleteDisciplina = (id) => http.delete(`/disciplina/${id}`)
const deleteTurma = (id) => http.delete(`/turma/${id}`)

// CREATE
const createProfe =(data) => http.post(`/profe`, data)
const createDisciplina =(data, config={}) => http.post(`/disciplina`, data, config)
const createTurma =(data, config={}) => http.post(`/turma`, data, config)

// UPDATE
const updateProfe = (id, data) => http.patch(`/profe/${id}`, data)
const updateDisciplina = (id, data) => http.patch(`/disciplina/${id}`, data)
const updateTurma = (id, data) => http.patch(`/turma/${id}`, data)

export {
    getProfe,
    getDisciplina,
    getTurma,
    deleteDisciplina,
    deleteProfe,
    deleteTurma,
    createProfe,
    createDisciplina,
    createTurma,
    updateProfe,
    updateDisciplina,
    updateTurma,
}