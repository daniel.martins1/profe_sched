import React from 'react'
import { Route, Router, Switch, Redirect } from 'react-router-dom'
import history from './config/history'

import Layout from './views/portal'
import Login from './views/portal/login'
import Logado from './views/portal/logado'
import Admin from './views/portal/admin'

import Disciplina from './views/portal/disciplina'
import Profe from './views/portal/profe'
import Turma from './views/portal/turma'


import { isAuthenticated } from './config/auth';
//import { isadmin } from './config/auth';


//admin
// import Landing from './views/admin/landing'
// import ProductsAdmin from './views/admin/products'
// import CategoriesAdmin from './views/admin/categories'

// const isadmin = () => false
const AdminRoute = ({ ...rest }) => {
    if (!isAuthenticated()) {
        return <Redirect to='/' />
    }
    // if (isadmin() === true) {
    //     return <Redirect to='/logado/admin' />
    // }
    // if (isAuthenticated()) {
    //     return <Redirect to='/logado' />
    // }
    
    return <Route {...rest} />
}


const Routers = () => (
    <Router history={history}>
        <Switch>
            <Route path="/">
                <Layout>
                    <Route basename="/" exact path="/" component={Login} />
                    <Route basename="/" exact path="/logado" component={Logado} />
                    <AdminRoute  exact path="/admin" component={Admin} />
                    <AdminRoute  exact path="/admin/profe" component={Profe} />
                    <AdminRoute  exact path="/admin/disciplina" component={Disciplina} />
                    <AdminRoute  exact path="/admin/turma" component={Turma} />
                </Layout>
                </Route>
   

            
        </Switch>

    </Router>

)

export default Routers




